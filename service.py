from datetime import datetime

# from werkzeug.contrib.fixers import ProxyFix
from flask import Flask, jsonify, request
app = Flask(__name__)
app.debug = True


def _get_logdata_params(request):
    unit_mac_address = request.values.get('m', None)
    firmware_version = request.values.get('v', None)
    sequential_number = request.values.get('s', None)
    server_datetime = datetime.utcnow()
    wifi_signal_strength = request.values.get('w', None)
    unit_temperature = request.values.get('t', None)
    a6 = request.values.get('a6', None)
    a7 = request.values.get('a7', None)
    in_decimal = request.values.get('ix', None)
    in_hex = request.values.get('ct', None)
    in_text = request.values.get('e', None)

    return (unit_mac_address,
            firmware_version,
            sequential_number,
            server_datetime,
            wifi_signal_strength,
            unit_temperature,
            a6, a7, in_decimal, in_hex, in_text)


def _get_startup_params(request):
    unit_mac_address = request.values.get('m', None)
    firmware_version = request.values.get('v', None)
    server_datetime = datetime.utcnow()
    param = request.values.get('c', None)
    data = request.values.get('d', None)

    return (unit_mac_address,
            firmware_version,
            server_datetime,
            param,
            data)


@app.route('/logdata', methods=['GET', 'POST'])
def logdata():
    try:
        params = _get_logdata_params(request)
        log = "%s V%s %s %s %s%% %s'F A %s %s [%s] %s %s\n" % params

        with open(params[0] + '.txt', 'a') as f:
            f.write(log)
    except IOError:
        return jsonify(status='Fail', msg='could not write to file')

    return jsonify(status='Success', msg='data written to file')


@app.route('/startup', methods=['GET', 'POST'])
def startup():
    try:
        params = _get_startup_params(request)
        log = "%s V%s %s %s = %s\n" % params

        with open(params[0] + '-config.txt', 'a') as f:
            f.write(log)
    except IOError:
        return jsonify(status='Fail', msg='could not write to file')

    return jsonify(status='Success', msg='data written to file')
    

# app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == "__main__":
    app.run(host='0.0.0.0')
