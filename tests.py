import unittest

from service import app

LOGDATA_PATH = '/logdata?m=ACCF2345B17A&v=0.60&s=3&' \
    'w=12&t=108.64&a6=470&a7=511&ix=3&ct=0xD0&e=RST_LLV_LPT'
STARTUP_PATH = 'startup?m=ACCF2345B17A&v=0.60&c=INFO&d=test'


class MyTestClass(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True


    def test_logdata_status_code(self):
        result = self.app.get(LOGDATA_PATH)
        self.assertEqual(result.status_code, 200)


    def test_startup_status_code(self):
        result = self.app.get(STARTUP_PATH)
        self.assertEqual(result.status_code, 200)


if __name__ == '__main__':
  unittest.main()